module Main where

import Test.HUnit
import System.Exit

import Test.Cases
import Test.Engine.Atualiza
import Test.Engine.Movimenta
import Test.Engine.Refresh
import Test.Maps.Build
import Test.Maps.Validation

tests = TestLabel "Micro Machines" $ TestList [
    testsEngineAtualiza,
    testsEngineMovimenta,
    testsEngineRefresh,
    testsMapsBuild,
    testsMapsValidation
  ]

main :: IO ()
main = do
          counts <- runTestTT tests
          if errors counts + failures counts == 0
            then exitSuccess
            else exitFailure