module Test.Cases where

import Test.HUnit
import Data.Game

double x = x + x

caminho00, caminho01, caminho02, caminho03, caminho04, caminho05, caminho06 :: Caminho
caminho00 = [Avanca,Avanca,CurvaDir,Sobe,CurvaDir,CurvaEsq,Avanca,CurvaDir,Avanca,Avanca,CurvaDir,Avanca,CurvaDir,CurvaEsq,Desce,CurvaDir]
caminho01 = [Avanca,CurvaDir,Sobe,CurvaDir,Avanca,CurvaDir,Desce,CurvaDir]
caminho02 = [CurvaDir,Avanca,Avanca,CurvaDir,CurvaEsq,CurvaDir,CurvaDir,Avanca,Avanca,Avanca,CurvaDir,Avanca]
caminho03 = [CurvaDir,CurvaEsq,Sobe,Avanca,Avanca,CurvaDir,Desce,CurvaEsq]
caminho04 = [Avanca,Avanca,CurvaDir,Avanca,Avanca,CurvaDir,Avanca,Avanca,CurvaDir,Avanca,Avanca,CurvaDir]
caminho05 = [Avanca,Avanca,CurvaDir,Avanca,Avanca,CurvaDir,Avanca,Avanca,Avanca,CurvaEsq,Avanca,Avanca,Avanca,CurvaEsq,Avanca,Avanca,CurvaEsq,Avanca,CurvaEsq,Avanca,CurvaDir,Avanca,Avanca,Avanca,Avanca,CurvaDir]
caminho06 = [Avanca,Avanca,CurvaDir,Sobe,CurvaDir,CurvaEsq,Avanca,CurvaDir,Avanca,Avanca,CurvaDir,Avanca,CurvaDir,CurvaEsq,Desce,CurvaDir]

caminho07, caminho08, caminho09, caminho10, caminho11, caminho12, caminho13 :: Caminho
caminho07 = [CurvaEsq,CurvaEsq,CurvaEsq,Avanca,Avanca,Desce,Sobe,CurvaEsq,CurvaDir,Desce,Sobe]
caminho08 = [Sobe,CurvaEsq,CurvaDir,Desce,Avanca,CurvaDir,Sobe,CurvaDir,Desce,CurvaEsq,Avanca,CurvaDir,Sobe,CurvaEsq,Desce,Avanca]
caminho09 = [CurvaDir,Sobe,Desce,CurvaDir,CurvaDir,Avanca,Sobe,Sobe,Desce,Avanca,CurvaDir,Desce,Avanca]
caminho10 = [Desce,Sobe,CurvaDir,Sobe,Sobe,CurvaEsq,Avanca,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,Avanca]
caminho11 = [Avanca,Avanca,CurvaEsq,Avanca,CurvaEsq,Avanca,CurvaEsq,Avanca,Avanca,Avanca,CurvaDir,Avanca,CurvaDir,Avanca,CurvaDir,Avanca]
caminho12 = [Sobe,Desce,Desce,Sobe,Sobe,Sobe,Desce,CurvaDir,CurvaEsq,Sobe,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq]
caminho13 = [CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq,CurvaEsq]

mapa00, mapa01, mapa02, mapa03, mapa04, mapa05, mapa06 :: Mapa
mapa00 = Mapa ((3,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 1,Peca (Curva Sul) 1,Peca Lava 0,Peca (Curva Norte) 1,Peca (Curva Sul) 1,Peca Lava 0],
    [Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 1,Peca Recta 1,Peca Recta 1,Peca (Curva Sul) 1,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa01 = Mapa ((2,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 1,Peca Recta 1,Peca (Curva Sul) 1,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa02 = Mapa ((3,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca (Curva Norte) 0,Peca (Curva Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste)0,Peca (Curva Sul) 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa03 = Mapa ((1,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Rampa Este) 0,Peca Recta 1,Peca Recta 1,Peca (Curva Este) 1,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Rampa Norte) 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa04 = Mapa ((2,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa05 = Mapa ((3,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa06 = Mapa ((3,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 1,Peca (Curva Sul) 1,Peca Lava 0,Peca (Curva Norte) 1,Peca (Curva Sul) 1,Peca Lava 0],
    [Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 1,Peca Recta 1,Peca Recta 1,Peca (Curva Sul) 1,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]

mapa07, mapa08, mapa09, mapa10, mapa11, mapa12, mapa13 :: Mapa
mapa07 = Mapa ((2,2),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Norte) (-1),Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Sul) (-1),Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Rampa Norte) (-1),Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) (-1),Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa08 = Mapa ((1,2),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca (Curva Norte) 1,Peca (Rampa Oeste) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Este) 0,Peca (Curva Sul) 1,Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Norte) 0,Peca (Rampa Este) 0,Peca (Curva Sul) 1,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 1,Peca (Rampa Oeste) 0,Peca (Curva Sul) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Norte) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa09 = Mapa ((2,4),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 1,Peca (Rampa Oeste) 0,Peca Recta 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 1,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Sul) 1,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Norte) 1,Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Norte) 0,Peca (Rampa Sul) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca (Rampa Norte) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Curva Sul) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa10 = Mapa ((1,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Oeste) (-1),Peca (Rampa Este) (-1),Peca (Curva Este) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Rampa Sul) 1,Peca (Curva Norte) 2,Peca Recta 2,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 2,Peca (Curva Oeste) 2,Peca (Curva Sul) 2,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa11 = Mapa ((3,3),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca Recta 0,Peca Recta 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Recta 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca Recta 0,Peca (Curva Sul) 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa12 = Mapa ((1,1),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Rampa Este) 0,Peca (Rampa Oeste) 0,Peca (Rampa Oeste) (-1),Peca (Rampa Este) (-1),Peca (Rampa Este) 0,Peca (Rampa Este) 1,Peca (Rampa Oeste) 1,Peca (Curva Este) 1,Peca (Curva Norte) 2,Peca (Curva Este) 2,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca (Curva Oeste) 1,Peca (Curva Oeste) 2,Peca (Curva Sul) 2,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
mapa13 = Mapa ((2,2),Este) [
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Norte) 0,Peca (Curva Este) 0,Peca Lava 0],
    [Peca Lava 0,Peca (Curva Oeste) 0,Peca (Curva Sul) 0,Peca Lava 0],
    [Peca Lava 0,Peca Lava 0,Peca Lava 0,Peca Lava 0]
  ]
