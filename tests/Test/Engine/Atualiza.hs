module Test.Engine.Atualiza where

import Test.HUnit
import Test.Cases
import Engine.Atualiza


testsEngineAtualiza = TestLabel "Engine.Atualiza" $ TestList [
    TestLabel "Teste 0" $ TestCase test0EngineAtualiza,
    TestLabel "Teste 1" $ TestCase test1EngineAtualiza,
    TestLabel "Teste 2" $ TestCase test2EngineAtualiza,
    TestLabel "Teste 3" $ TestCase test3EngineAtualiza,
    TestLabel "Teste 4" $ TestCase test4EngineAtualiza,
    TestLabel "Teste 5" $ TestCase test5EngineAtualiza,
    TestLabel "Teste 6" $ TestCase test6EngineAtualiza
  ]

test0EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
test1EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
test2EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
test3EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
test4EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
test5EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
test6EngineAtualiza = assertEqual "for (double 3)," 6 (double 3)
