module Test.Maps.Build where

import Test.HUnit
import Test.Cases
import Data.Game
import Maps.Build

testsMapsBuild = TestLabel "Maps.Build" $ TestList [
    TestLabel "Teste  0" $ TestCase test00MapsBuild,
    TestLabel "Teste  1" $ TestCase test01MapsBuild,
    TestLabel "Teste  2" $ TestCase test02MapsBuild,
    TestLabel "Teste  3" $ TestCase test03MapsBuild,
    TestLabel "Teste  4" $ TestCase test04MapsBuild,
    TestLabel "Teste  5" $ TestCase test05MapsBuild,
    TestLabel "Teste  6" $ TestCase test06MapsBuild,
    TestLabel "Teste  7" $ TestCase test07MapsBuild,
    TestLabel "Teste  8" $ TestCase test08MapsBuild,
    TestLabel "Teste  9" $ TestCase test09MapsBuild,
    TestLabel "Teste 10" $ TestCase test10MapsBuild,
    TestLabel "Teste 11" $ TestCase test11MapsBuild,
    TestLabel "Teste 12" $ TestCase test12MapsBuild,
    TestLabel "Teste 12" $ TestCase test13MapsBuild
  ]

test00MapsBuild = assertEqual "constroi caminho00:" mapa00 (constroi caminho00)
test01MapsBuild = assertEqual "constroi caminho01:" mapa01 (constroi caminho01)
test02MapsBuild = assertEqual "constroi caminho02:" mapa02 (constroi caminho02)
test03MapsBuild = assertEqual "constroi caminho03:" mapa03 (constroi caminho03)
test04MapsBuild = assertEqual "constroi caminho04:" mapa04 (constroi caminho04)
test05MapsBuild = assertEqual "constroi caminho05:" mapa05 (constroi caminho05)
test06MapsBuild = assertEqual "constroi caminho06:" mapa06 (constroi caminho06)

test07MapsBuild = assertEqual "constroi caminho07:" mapa07 (constroi caminho07)
test08MapsBuild = assertEqual "constroi caminho08:" mapa08 (constroi caminho08)
test09MapsBuild = assertEqual "constroi caminho09:" mapa09 (constroi caminho09)
test10MapsBuild = assertEqual "constroi caminho10:" mapa10 (constroi caminho10)
test11MapsBuild = assertEqual "constroi caminho11:" mapa11 (constroi caminho11)
test12MapsBuild = assertEqual "constroi caminho12:" mapa12 (constroi caminho12)
test13MapsBuild = assertEqual "constroi caminho13:" mapa13 (constroi caminho13)