module Test.Maps.Validation where

import Test.HUnit
import Test.Cases
import Maps.Validation

testsMapsValidation = TestLabel "Maps.Validation" $ TestList [
    TestLabel "Teste  0" $ TestCase test00MapsValidation,
    TestLabel "Teste  1" $ TestCase test01MapsValidation,
    TestLabel "Teste  2" $ TestCase test02MapsValidation,
    TestLabel "Teste  3" $ TestCase test03MapsValidation,
    TestLabel "Teste  4" $ TestCase test04MapsValidation,
    TestLabel "Teste  5" $ TestCase test05MapsValidation,
    TestLabel "Teste  6" $ TestCase test06MapsValidation,
    TestLabel "Teste  7" $ TestCase test07MapsValidation,
    TestLabel "Teste  8" $ TestCase test08MapsValidation,
    TestLabel "Teste  9" $ TestCase test09MapsValidation,
    TestLabel "Teste 10" $ TestCase test10MapsValidation,
    TestLabel "Teste 11" $ TestCase test11MapsValidation,
    TestLabel "Teste 12" $ TestCase test12MapsValidation,
    TestLabel "Teste 13" $ TestCase test13MapsValidation
  ]

test00MapsValidation = assertEqual "valida mapa00:" True  (valida mapa00)
test01MapsValidation = assertEqual "valida mapa01:" True  (valida mapa01)
test02MapsValidation = assertEqual "valida mapa02:" True  (valida mapa02)
test03MapsValidation = assertEqual "valida mapa03:" False (valida mapa03)
test04MapsValidation = assertEqual "valida mapa04:" True  (valida mapa04)
test05MapsValidation = assertEqual "valida mapa05:" True  (valida mapa05)
test06MapsValidation = assertEqual "valida mapa06:" True  (valida mapa06)

test07MapsValidation = assertEqual "valida mapa07:" False (valida mapa07)
test08MapsValidation = assertEqual "valida mapa08:" False (valida mapa08)
test09MapsValidation = assertEqual "valida mapa09:" False (valida mapa09)
test10MapsValidation = assertEqual "valida mapa10:" False (valida mapa10)
test11MapsValidation = assertEqual "valida mapa11:" True  (valida mapa11)
test12MapsValidation = assertEqual "valida mapa12:" False (valida mapa12)
test13MapsValidation = assertEqual "valida mapa13:" True  (valida mapa13)
