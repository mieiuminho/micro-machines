{-|
Module      : Engine.Refresh
Description : State update
Copyright   : Nelson Estevão <hello@estevao.xyz>;
            : Pedro Ribeiro  <>;
            : Rui Mendes <>;

//TODO: Add module description
-}

module Engine.Refresh where

import Data.Game
import Data.Helper
import Data.Maybe
import Engine.Atualiza
import Engine.Movimenta

-- | Função que através das acções dos jogadores atualiza o jogo.
atualizaMovimenta :: Tempo  -- ^ a duração do evento
                  -> Jogo   -- ^ o estado do jogo
                  -> [Acao] -- ^ a lista de ações tomadas pelos jogadores
                  -> Jogo   -- ^ o estado atualizado do jogo
atualizaMovimenta t j as = movimentaTodos t va -- posições atualizadas
                        where
                          va = atualizaTodos t j 0 as -- vetores atualizados

-- | Função que através das acções, atualiza o jogo sem movimentar os carros.
atualizaTodos :: Tempo  -- ^ a duração do evento
              -> Jogo   -- ^ o estado do jogo
              -> Int    -- ^ indice do primeiro jogador
              -> [Acao] -- ^ a lista de ações tomadas pelos jogadores
              -> Jogo
atualizaTodos t e n [a] = atualiza t e n a
atualizaTodos t e n (a:as) = atualizaTodos t (atualiza t e n a) (n+1) as

-- | Função que através dos vetores velocidades, movimenta os carros.
movimentaTodos :: Tempo -- ^ a duração do evento
               -> Jogo  -- ^ o estado do jogo com os vetores atualizados
               -> Jogo
movimentaTodos t e@(Jogo m@(Mapa w tb) p cs ns hs) = Jogo m p ncs ns hs
              where
                mcs = map (movimenta tb t) cs -- criada lista Maybe Carro
                ncs = removeMaybes mcs hs     -- lista de carros nas posições

-- | Função que de uma lista de Maybe Carros, gera uma lista de Carros restaurando os carros que morreram no centro da última peça em que estiveram.
removeMaybes :: [Maybe Carro] -> [[Posicao]] -> [Carro]
removeMaybes [] _ = []
removeMaybes (Nothing:ms) (h:hs) = Carro (mapPair (0.5 +) pt) 0 (0,0):removeMaybes ms hs
          where pt = mapPair fromIntegral (head h)
removeMaybes (m:ms) (h:hs) = fromJust m:removeMaybes ms hs
