{-|
Module      : Maps.Build
Description : Build Maps from a Path
Copyright   : Nelson Estevão <hello@estevao.xyz>;
            : Pedro Ribeiro  <>;
            : Rui Mendes <>;

//TODO: Add module description
-}

module Maps.Build where

import Data.Game

-- | Função que gera um /Tabuleiro/ só com peças do tipo lava.
constroiTabuleiroBase :: Caminho -> Tabuleiro
constroiTabuleiroBase c = replicate y $ replicate x (Peca Lava 0)
                        where (x,y) = dimensao c

--------------------------------------------------------------------------------
-- Etapa 2 --
--------------------------------------------------------------------------------

{- | Função que recebendo um /Caminho/ e uma /Altura/ inicial, retorna a lista de alturas que as peças do percurso irão tomar. -}
listaAlturas :: Caminho -> Altura -> [Altura]
listaAlturas [] _ = []
listaAlturas (p:ps) h | p == Desce = (h-1):listaAlturas ps (h-1)
                      | p == Sobe  = h:listaAlturas ps (h+1)
                      | otherwise  = h:listaAlturas ps h

{- | Função que recebe uma orientação atual e consoante o próximo /Passo/ retorna a nova /Orientacao/ atual. -}
auxiliarOrientacoes :: Passo -> Orientacao -> Orientacao
auxiliarOrientacoes p Norte | p == CurvaDir = Este
                            | p == CurvaEsq = Oeste
auxiliarOrientacoes p Este  | p == CurvaDir = Sul
                            | p == CurvaEsq = Norte
auxiliarOrientacoes p Sul   | p == CurvaDir = Oeste
                            | p == CurvaEsq = Este
auxiliarOrientacoes p Oeste | p == CurvaDir = Norte
                            | p == CurvaEsq = Sul
auxiliarOrientacoes p o = o

{- | Função que recebe o /Caminho/ geral e a orientação inical, resultando numa lista com todas as orientações de cada peça do percurso. Para tal usa a `auxiliarOrientacoes` e a `listaOrientacoes` -}
listaOrientacoes :: Caminho -> Orientacao -> [Orientacao]
listaOrientacoes [] _ = []
listaOrientacoes (p:ps) o = auxiliarOrientacoes p o: listaOrientacoes ps (auxiliarOrientacoes p o)

{- | Função que recebe a orientação que o /Passo/ que irá tomar, assim como a altura, retornando a peça correspondente a tal movimento. -}
auxiliarListaPecas :: Orientacao -> Passo -> Altura -> Peca
auxiliarListaPecas Este  p h | p == Desce = Peca (Rampa Oeste) h
                             | p == CurvaEsq = Peca (Curva Sul) h
auxiliarListaPecas Norte p h | p == Desce = Peca (Rampa Sul) h
                             | p == CurvaEsq = Peca (Curva Este) h
auxiliarListaPecas Oeste p h | p == Desce = Peca (Rampa Este) h
                             | p == CurvaEsq = Peca (Curva Norte) h
auxiliarListaPecas Sul   p h | p == Desce = Peca (Rampa Norte) h
                             | p == CurvaEsq = Peca (Curva Oeste) h
auxiliarListaPecas o     p h | p == Avanca = Peca Recta h
                             | p == Sobe = Peca (Rampa o) h
                             | p == CurvaDir = Peca (Curva o) h


{- | Função que recebendo a lista de orientações que vai tomar, de acordo com o /Caminho/ fornecido, e a altura inicial, retorna a lista de peças que representam esse /Caminho/. -}
auxiliarListaPecas2 :: [Orientacao] -> Caminho -> [Altura] -> [Peca]
auxiliarListaPecas2 [] _ _ = []
auxiliarListaPecas2 _ [] _ = []
auxiliarListaPecas2 (o:os) (p:ps) (a:as) = auxiliarListaPecas o p a : auxiliarListaPecas2 os ps as

{- | Função que recebendo a lista de orientacções a tomar, a lista de alturas a tomar, devolve a lista de peças resultantes. Para tal utiliza as funções `auxiliarListaPecas` e `auxiliarListaPecas2`. -}
listaPecas :: [Orientacao] -> Caminho -> [Altura] -> [Peca]
listaPecas [] _ _ = []
listaPecas _ [] _ = []
listaPecas (o:os) (p:ps) (a:as) = auxiliarListaPecas Este p a : auxiliarListaPecas2 (o:os) ps as

--------------------------------------------------------------------------------
-- Etapa 3 --
--------------------------------------------------------------------------------

{- | Função que recebe um índice matricial e uma orientação, retorna o índice seguinte. -}
auxiliarListaIndices :: Posicao -> Orientacao -> Posicao
auxiliarListaIndices (x,y) Norte = (x,y-1)
auxiliarListaIndices (x,y) Sul   = (x,y+1)
auxiliarListaIndices (x,y) Este  = (x+1,y)
auxiliarListaIndices (x,y) Oeste = (x-1,y)

{- | Função que gera a partir das Orientações tomadas por um caminho
e a posição inicial, gera a lista das posições tomadas por esse caminho. -}
listaIndices :: [Orientacao] -> Posicao -> [Posicao]
listaIndices [] _ = []
listaIndices (o:os) (x,y) = (x,y):listaIndices os p
                          where
                            p = auxiliarListaIndices (x,y) o

{- | Função que recebe uma peça, a posição em que se quer essa peça, e substitui-a no tabuleiro dado. -}
auxiliarConstroiTabuleiro :: Peca -> Posicao -> Tabuleiro -> Tabuleiro
auxiliarConstroiTabuleiro pec (x,y) ((pc:pcs):tbs) | y == 0 && x == 0 = (pec:pcs):tbs
                                                   | y >  0           = (pc:pcs):auxiliarConstroiTabuleiro pec (x,y-1) tbs
                                                   | x >  0           = inserir pc (auxiliarConstroiTabuleiro pec (x-1,y) (pcs:tbs))
                                                  where
                                                    inserir x ((y:ys):resto) = (x:y:ys):resto

{- | Função que recebendo uma lista de peças do percurso, e as posições
 a que correspondem substitui no tabuleiro dado cada um delas. -}
constroiTabuleiro :: [Peca] -> [Posicao] -> Tabuleiro -> Tabuleiro
constroiTabuleiro [] _ tbs = tbs
constroiTabuleiro (pec:pecs) ((x,y):xys) tbs = constroiTabuleiro pecs xys (auxiliarConstroiTabuleiro pec (x,y) tbs)


{- | A função `constroi`, recebe um /Caminho/ e gera um __/Mapa/__. Sendo por isso, a /Função Objetivo/ da __Tarefa 1__. -}
constroi :: Caminho -> Mapa
constroi c = Mapa ((x,y),o) tb
          where
            (x,y) = partida c
            o     = dirInit
            os    = listaOrientacoes c o
            a     = altInit
            as    = listaAlturas c a
            xsys  = listaIndices os (x,y)
            pcs   = listaPecas os c as
            tbB   = constroiTabuleiroBase c
            tb    = constroiTabuleiro pcs xsys tbB
