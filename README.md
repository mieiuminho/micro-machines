[trello]: https://trello.com/b/pQNROiMB
[semaphoreci]: https://semaphoreci.com/mieiuminho/micro-machines
[haskell-platform]: https://www.haskell.org/platform/

# Micro Machines
[![Trello](https://img.shields.io/badge/trello-board-blue.svg?style=flat-square)][trello]
[![Build Status](https://semaphoreci.com/api/v1/mieiuminho/micro-machines/branches/master/badge.svg)][semaphoreci]

## Setup

Make sure you have `haskell-platform` installed. You can read more
[here][haskell-platform]. After that, you need to install all the dependencies
of the project. Just run `cabal install --only-depencies` once.

### Build

In order to build, just run `cabal build`. Play by running `cabal run`.

### Documentation

You can generate the documentation by running `cabal haddock --executables`.

### Testing

To run the tests type `cabal test`. If you are having problems, you can run 
`cabal configure --enable-tests` before.

## Team

![Nelson Estevão][nelson-pic] | ![Pedro Ribeiro][pedro-pic] | ![Rui Mendes][rui-pic]
:---: | :---: | :---:
[Nelson Estevão][nelson] | [Pedro Ribeiro][pedro] | [Rui Mendes][rui]

[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120
[rui]: https://github.com/ruimendes29
[rui-pic]: https://github.com/ruimendes29.png?size=120
